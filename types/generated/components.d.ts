import type { Attribute, Schema } from '@strapi/strapi';

export interface ComponentAtlas extends Schema.Component {
  collectionName: 'c_component_atlases';
  info: {
    displayName: 'Map';
    icon: 'map-marker-alt';
    name: 'atlas';
  };
  attributes: {
    childIdentifier: Attribute.String & Attribute.Required;
    childType: Attribute.String & Attribute.Required;
    config: Attribute.JSON & Attribute.Required;
  };
}

export interface ComponentAudioPlayer extends Schema.Component {
  collectionName: 'c_component_audio_players';
  info: {
    displayName: 'Audio player';
    icon: 'music';
    name: 'audio-player';
  };
  attributes: {
    items: Attribute.Component<'config.media-translation', true>;
  };
}

export interface ComponentCollapsibleText extends Schema.Component {
  collectionName: 'c_component_collapsible_texts';
  info: {
    displayName: 'Collapsible text';
    icon: 'angle-down';
    name: 'collapsible-text';
  };
  attributes: {
    text: Attribute.Component<'config.long-translation', true>;
    title: Attribute.Component<'config.short-translation', true>;
    titleLevel: Attribute.Enumeration<['2', '3', '4', '5', '6']> &
      Attribute.DefaultTo<'2'>;
  };
}

export interface ComponentCollapsibleTextWithLead extends Schema.Component {
  collectionName: 'c_component_expandable_descriptions';
  info: {
    description: '';
    displayName: 'Collapsible text with lead';
    icon: 'arrows-alt-v';
    name: 'collapsible-text-with-lead';
  };
  attributes: {
    lead: Attribute.Component<'config.long-translation', true>;
    style: Attribute.Enumeration<['primary', 'secondary']> &
      Attribute.DefaultTo<'primary'>;
    text: Attribute.Component<'config.long-translation', true>;
    title: Attribute.Component<'config.short-translation', true>;
    titleLevel: Attribute.Enumeration<['2', '3', '4', '5', '6']> &
      Attribute.DefaultTo<'2'>;
  };
}

export interface ComponentDemandItem extends Schema.Component {
  collectionName: 'c_component_demand_items';
  info: {
    displayName: 'Demand item';
    icon: 'bullhorn';
    name: 'demand-item';
  };
  attributes: {
    icon: Attribute.Component<'config.image-translation', true>;
    lead: Attribute.Component<'config.medium-translation', true>;
    text: Attribute.Component<'config.long-translation', true>;
    title: Attribute.Component<'config.short-translation', true>;
  };
}

export interface ComponentDownloadSection extends Schema.Component {
  collectionName: 'c_component_download_sections';
  info: {
    displayName: 'Download section';
    icon: 'cloud-download-alt';
    name: 'download-section';
  };
  attributes: {
    media: Attribute.Component<'config.media', true>;
  };
}

export interface ComponentEvents extends Schema.Component {
  collectionName: 'c_component_events';
  info: {
    displayName: 'Events';
    icon: 'calendar-day';
    name: 'events';
  };
  attributes: {
    childIdentifier: Attribute.String & Attribute.Required;
    childType: Attribute.String & Attribute.Required;
    config: Attribute.JSON;
  };
}

export interface ComponentFactItem extends Schema.Component {
  collectionName: 'c_component_fact_items';
  info: {
    description: '';
    displayName: 'Fact item';
    icon: 'atom';
    name: 'fact-item';
  };
  attributes: {
    icon: Attribute.Component<'config.image-translation', true>;
    lead: Attribute.Component<'config.medium-translation', true>;
    links: Attribute.Component<'config.button', true> &
      Attribute.SetMinMax<
        {
          max: 4;
        },
        number
      >;
    title: Attribute.Component<'config.short-translation', true>;
  };
}

export interface ComponentForm extends Schema.Component {
  collectionName: 'c_config_forms';
  info: {
    displayName: 'Form';
    icon: 'question-circle';
    name: 'form';
  };
  attributes: {
    fields: Attribute.Component<'config.field', true> & Attribute.Required;
    identifier: Attribute.String & Attribute.Required & Attribute.Unique;
    submitText: Attribute.Component<'config.short-translation', true>;
    title: Attribute.Component<'config.short-translation', true>;
  };
}

export interface ComponentHeadline extends Schema.Component {
  collectionName: 'c_component_headlines';
  info: {
    displayName: 'Headline';
    icon: 'heading';
    name: 'headline';
  };
  attributes: {
    link: Attribute.Component<'config.button'>;
    subtitle: Attribute.Component<'config.short-translation', true>;
    text: Attribute.Component<'config.long-translation', true>;
    title: Attribute.Component<'config.short-translation', true>;
  };
}

export interface ComponentIconTitle extends Schema.Component {
  collectionName: 'c_component_icon_titles';
  info: {
    displayName: 'Icon title';
    icon: 'indent';
    name: 'icon-title';
  };
  attributes: {
    icon: Attribute.Component<'config.image-translation', true>;
    text: Attribute.Component<'config.medium-translation', true>;
    title: Attribute.Component<'config.short-translation', true>;
  };
}

export interface ComponentIframe extends Schema.Component {
  collectionName: 'c_component_iframes';
  info: {
    displayName: 'Iframe';
    icon: 'expand';
    name: 'iframe';
  };
  attributes: {
    html: Attribute.Component<'config.medium-translation', true>;
    src: Attribute.Component<'config.short-translation', true>;
  };
}

export interface ComponentJoinItem extends Schema.Component {
  collectionName: 'c_component_join_items';
  info: {
    displayName: 'Join item';
    icon: 'hands-helping';
    name: 'join-item';
  };
  attributes: {
    icon: Attribute.Component<'config.image-translation', true>;
    image: Attribute.Component<'config.image-translation', true>;
    link: Attribute.Component<'config.button'>;
    text: Attribute.Component<'config.medium-translation', true>;
    title: Attribute.Component<'config.short-translation', true>;
  };
}

export interface ComponentLandingLink extends Schema.Component {
  collectionName: 'c_component_landing_links';
  info: {
    displayName: 'Landing link';
    icon: 'external-link-alt';
    name: 'landing-link';
  };
  attributes: {
    icon: Attribute.Component<'config.image-translation', true>;
    link: Attribute.Component<'config.button'>;
    text: Attribute.Component<'config.medium-translation', true>;
    title: Attribute.Component<'config.short-translation', true>;
    titleLevel: Attribute.Enumeration<['2', '3', '4', '5', '6']> &
      Attribute.DefaultTo<'2'>;
  };
}

export interface ComponentLargeImage extends Schema.Component {
  collectionName: 'c_component_image_with_captions';
  info: {
    displayName: 'Large image';
    icon: 'file-image';
    name: 'large-image';
  };
  attributes: {
    image: Attribute.Component<'config.image-translation', true>;
  };
}

export interface ComponentMilestones extends Schema.Component {
  collectionName: 'c_component_milestones';
  info: {
    displayName: 'Milestones';
    icon: 'check-square';
    name: 'milestones';
  };
  attributes: {
    backgroundLarge: Attribute.Component<'config.image-translation', true>;
    backgroundSmall: Attribute.Component<'config.image-translation', true>;
    entries: Attribute.Component<'config.milestones-entry', true>;
    title: Attribute.Component<'config.short-translation', true>;
  };
}

export interface ComponentNewsletterForm extends Schema.Component {
  collectionName: 'c_component_newsletter_forms';
  info: {
    displayName: 'Newsletter form';
    icon: 'envelope-open';
    name: 'newsletter-form';
  };
  attributes: {
    identifier: Attribute.String;
    submitUrl: Attribute.String;
    text: Attribute.Component<'component.titled-text'>;
  };
}

export interface ComponentPetition extends Schema.Component {
  collectionName: 'c_component_petitions';
  info: {
    description: '';
    displayName: 'Petition';
    icon: 'pen-nib';
    name: 'petition';
  };
  attributes: {
    form: Attribute.Component<'component.form'>;
    shareButtons: Attribute.Component<'component.share-buttons'>;
    text: Attribute.Component<'config.long-translation', true>;
    title: Attribute.Component<'config.short-translation', true>;
  };
}

export interface ComponentPostSection extends Schema.Component {
  collectionName: 'c_component_post_sections';
  info: {
    displayName: 'Post section';
    icon: 'receipt';
    name: 'post-section';
  };
  attributes: {
    childIdentifier: Attribute.String;
    childType: Attribute.String & Attribute.Required;
    config: Attribute.JSON;
  };
}

export interface ComponentPosts extends Schema.Component {
  collectionName: 'c_component_posts';
  info: {
    displayName: 'Posts';
    icon: 'file-alt';
    name: 'posts';
  };
  attributes: {
    childIdentifier: Attribute.String & Attribute.Required;
    childType: Attribute.String;
    config: Attribute.JSON;
  };
}

export interface ComponentRaiseNowDonationWidget extends Schema.Component {
  collectionName: 'c_component_raise_now_donation_widget';
  info: {
    displayName: 'Raise now donation widget';
    icon: 'credit-card';
    name: 'raise-now-donation-widget';
  };
  attributes: {
    config: Attribute.JSON & Attribute.Required;
    lead: Attribute.Component<'config.medium-translation', true>;
    scriptSrc: Attribute.String & Attribute.Required;
    title: Attribute.Component<'config.short-translation', true>;
  };
}

export interface ComponentReferences extends Schema.Component {
  collectionName: 'c_component_references';
  info: {
    displayName: 'References';
    icon: 'info';
    name: 'references';
  };
  attributes: {
    references: Attribute.Component<'config.reference', true>;
    text: Attribute.Component<'config.long-translation', true>;
  };
}

export interface ComponentRichText extends Schema.Component {
  collectionName: 'c_component_markdowns';
  info: {
    displayName: 'Rich text';
    icon: 'arrow-circle-down';
    name: 'rich-text';
  };
  attributes: {
    text: Attribute.Component<'config.long-translation', true>;
  };
}

export interface ComponentShareButtons extends Schema.Component {
  collectionName: 'c_component_share_buttons';
  info: {
    displayName: 'Share buttons';
    icon: 'share-alt-square';
    name: 'shareButtons';
  };
  attributes: {
    callToAction: Attribute.Component<'config.short-translation', true>;
    emailMessage: Attribute.Component<'config.medium-translation', true>;
    emailSubject: Attribute.Component<'config.short-translation', true>;
    enableFacebook: Attribute.Boolean;
    twitterMessage: Attribute.Component<'config.medium-translation', true>;
    urlOverride: Attribute.String;
    whatsappMessage: Attribute.Component<'config.medium-translation', true>;
  };
}

export interface ComponentSlides extends Schema.Component {
  collectionName: 'c_component_slides';
  info: {
    displayName: 'Slides';
    icon: 'photo-video';
    name: 'slides';
  };
  attributes: {
    items: Attribute.Component<'config.slide', true>;
  };
}

export interface ComponentTitle extends Schema.Component {
  collectionName: 'c_component_titles';
  info: {
    displayName: 'Title';
    icon: 'bold';
    name: 'title';
  };
  attributes: {
    title: Attribute.Component<'config.short-translation', true>;
    titleLevel: Attribute.Enumeration<['2', '3', '4', '5', '6']> &
      Attribute.DefaultTo<'2'>;
  };
}

export interface ComponentTitledSubtitledIconItem extends Schema.Component {
  collectionName: 'c_component_titled_subtitled_icon_items';
  info: {
    displayName: 'Titled/subtitled icon item';
    icon: 'equals';
    name: 'titled-subtitled-icon-item';
  };
  attributes: {
    icon: Attribute.Component<'config.image-translation', true>;
    subtitle: Attribute.Component<'config.short-translation', true>;
    text: Attribute.Component<'config.long-translation', true>;
    title: Attribute.Component<'config.short-translation', true>;
  };
}

export interface ComponentTitledText extends Schema.Component {
  collectionName: 'c_component_texts';
  info: {
    displayName: 'Titled text';
    icon: 'text-height';
    name: 'titled-text';
  };
  attributes: {
    text: Attribute.Component<'config.long-translation', true>;
    title: Attribute.Component<'config.short-translation', true>;
  };
}

export interface ConfigButton extends Schema.Component {
  collectionName: 'c_config_links';
  info: {
    displayName: 'Button';
    icon: 'link';
    name: 'button';
  };
  attributes: {
    link: Attribute.Component<'config.short-translation', true>;
    style: Attribute.Enumeration<['primary', 'secondary']> &
      Attribute.DefaultTo<'primary'>;
    text: Attribute.Component<'config.short-translation', true>;
    type: Attribute.Enumeration<
      [
        'read-more',
        'more',
        'facts',
        'events',
        'join',
        'demands',
        'movement',
        'facebook',
        'website'
      ]
    >;
  };
}

export interface ConfigField extends Schema.Component {
  collectionName: 'c_config_fields';
  info: {
    description: '';
    displayName: 'Field';
    icon: 'question';
    name: 'field';
  };
  attributes: {
    config: Attribute.JSON;
    key: Attribute.String & Attribute.Required;
    placeholder: Attribute.Component<'config.short-translation', true>;
    required: Attribute.Boolean &
      Attribute.Required &
      Attribute.DefaultTo<false>;
    type: Attribute.Enumeration<
      [
        'text',
        'email',
        'zipch',
        'phone',
        'number',
        'dropdown',
        'checkboxes',
        'boolean'
      ]
    > &
      Attribute.Required;
  };
}

export interface ConfigImageTranslation extends Schema.Component {
  collectionName: 'c_config_image_translations';
  info: {
    description: '';
    displayName: 'Image translation';
    icon: 'images';
    name: 'image-translation';
  };
  attributes: {
    image: Attribute.Media<'images'>;
    language: Attribute.Enumeration<['all', 'de', 'fr', 'it']> &
      Attribute.Required &
      Attribute.DefaultTo<'all'>;
  };
}

export interface ConfigLongTranslation extends Schema.Component {
  collectionName: 'c_config_long_translations';
  info: {
    displayName: 'Long translation';
    icon: 'book';
    name: 'long-translation';
  };
  attributes: {
    language: Attribute.Enumeration<['all', 'de', 'fr', 'it']> &
      Attribute.Required &
      Attribute.DefaultTo<'all'>;
    text: Attribute.RichText & Attribute.Required;
  };
}

export interface ConfigMedia extends Schema.Component {
  collectionName: 'c_config_media';
  info: {
    displayName: 'Media';
    icon: 'archive';
    name: 'media';
  };
  attributes: {
    item: Attribute.Component<'config.media-translation', true>;
    title: Attribute.Component<'config.short-translation', true>;
  };
}

export interface ConfigMediaTranslation extends Schema.Component {
  collectionName: 'c_config_media_translations';
  info: {
    description: '';
    displayName: 'Media (images, files, ...) translations';
    icon: 'file-medical-alt';
    name: 'media-translation';
  };
  attributes: {
    language: Attribute.Enumeration<['all', 'de', 'fr', 'it']> &
      Attribute.Required &
      Attribute.DefaultTo<'all'>;
    medium: Attribute.Media<'images' | 'files' | 'videos' | 'audios'>;
  };
}

export interface ConfigMediumTranslation extends Schema.Component {
  collectionName: 'c_config_medium_translations';
  info: {
    displayName: 'Medium translation';
    icon: 'clipboard';
    name: 'medium-translation';
  };
  attributes: {
    language: Attribute.Enumeration<['all', 'de', 'fr', 'it']> &
      Attribute.Required &
      Attribute.DefaultTo<'all'>;
    text: Attribute.Text;
  };
}

export interface ConfigMilestonesEntry extends Schema.Component {
  collectionName: 'c_config_milestones_entries';
  info: {
    displayName: 'Milestone entry';
    icon: 'check';
    name: 'milestones-entry';
  };
  attributes: {
    date: Attribute.Date & Attribute.Required;
    text: Attribute.Component<'config.short-translation', true>;
  };
}

export interface ConfigReference extends Schema.Component {
  collectionName: 'c_config_references';
  info: {
    displayName: 'References';
    icon: 'file-export';
    name: 'reference';
  };
  attributes: {
    description: Attribute.Component<'config.medium-translation', true>;
    link: Attribute.Component<'config.short-translation', true>;
  };
}

export interface ConfigShortTranslation extends Schema.Component {
  collectionName: 'c_config_short_translations';
  info: {
    displayName: 'Short translation';
    icon: 'language';
    name: 'short-translation';
  };
  attributes: {
    language: Attribute.Enumeration<['all', 'de', 'fr', 'it']> &
      Attribute.Required &
      Attribute.DefaultTo<'all'>;
    text: Attribute.String & Attribute.Required;
  };
}

export interface ConfigSlide extends Schema.Component {
  collectionName: 'c_config_slide';
  info: {
    description: '';
    displayName: 'Slide';
    icon: 'chevron-right';
    name: 'slide';
  };
  attributes: {
    image: Attribute.Component<'config.image-translation', true>;
    text: Attribute.Component<'config.medium-translation', true>;
  };
}

declare module '@strapi/types' {
  export module Shared {
    export interface Components {
      'component.atlas': ComponentAtlas;
      'component.audio-player': ComponentAudioPlayer;
      'component.collapsible-text': ComponentCollapsibleText;
      'component.collapsible-text-with-lead': ComponentCollapsibleTextWithLead;
      'component.demand-item': ComponentDemandItem;
      'component.download-section': ComponentDownloadSection;
      'component.events': ComponentEvents;
      'component.fact-item': ComponentFactItem;
      'component.form': ComponentForm;
      'component.headline': ComponentHeadline;
      'component.icon-title': ComponentIconTitle;
      'component.iframe': ComponentIframe;
      'component.join-item': ComponentJoinItem;
      'component.landing-link': ComponentLandingLink;
      'component.large-image': ComponentLargeImage;
      'component.milestones': ComponentMilestones;
      'component.newsletter-form': ComponentNewsletterForm;
      'component.petition': ComponentPetition;
      'component.post-section': ComponentPostSection;
      'component.posts': ComponentPosts;
      'component.raise-now-donation-widget': ComponentRaiseNowDonationWidget;
      'component.references': ComponentReferences;
      'component.rich-text': ComponentRichText;
      'component.share-buttons': ComponentShareButtons;
      'component.slides': ComponentSlides;
      'component.title': ComponentTitle;
      'component.titled-subtitled-icon-item': ComponentTitledSubtitledIconItem;
      'component.titled-text': ComponentTitledText;
      'config.button': ConfigButton;
      'config.field': ConfigField;
      'config.image-translation': ConfigImageTranslation;
      'config.long-translation': ConfigLongTranslation;
      'config.media': ConfigMedia;
      'config.media-translation': ConfigMediaTranslation;
      'config.medium-translation': ConfigMediumTranslation;
      'config.milestones-entry': ConfigMilestonesEntry;
      'config.reference': ConfigReference;
      'config.short-translation': ConfigShortTranslation;
      'config.slide': ConfigSlide;
    }
  }
}
